package com.example.threeinrow.fragment;

import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.example.threeinrow.R;
import com.example.threeinrow.util.Util;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

public class MenuFragment extends Fragment {
    private View mView;
    private Target target;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_menu, container, false);
        setBackground();

        FragmentTransaction ft = getParentFragmentManager().beginTransaction();
        Button newGame = mView.findViewById(R.id.btn_menu_new_game);
        newGame.setOnClickListener(v -> ft.replace(R.id.fragmentContainerView, new GameFragment())
                .addToBackStack(null).commit());
        Button records = mView.findViewById(R.id.btn_menu_records);
        records.setOnClickListener(v -> ft.replace(R.id.fragmentContainerView, new RecordsFragment())
                .addToBackStack(null).commit());
        Button settings = mView.findViewById(R.id.btn_menu_settings);
        settings.setOnClickListener(v -> ft.replace(R.id.fragmentContainerView, new SettingsFragment())
                .addToBackStack(null).commit());
        return mView;
    }

    private void setBackground() {
        Bitmap background = Util.getBitmap(getContext(), "background.png");
        ConstraintLayout back = mView.findViewById(R.id.layout_menu);
        if(background != null) back.setBackground(new BitmapDrawable(getResources(), background));
        else {
            target = new Target() {
                @Override
                public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                    back.setBackground(new BitmapDrawable(getResources(), bitmap));
                    Util.saveBitmap(getContext(), bitmap, "background.png", success -> {
                    });
                }

                @Override
                public void onBitmapFailed(Exception e, Drawable errorDrawable) {

                }

                @Override
                public void onPrepareLoad(Drawable placeHolderDrawable) {

                }
            };
            Picasso.get().load("http://159.69.89.54/ThreeInRow/backgrounds/default.png").into(target);
        }
    }
}