package com.example.threeinrow.fragment;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;

import androidx.activity.OnBackPressedCallback;
import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

import com.example.threeinrow.R;
import com.example.threeinrow.data.Record;
import com.example.threeinrow.mvp.RecordsPresenter;
import com.example.threeinrow.mvp.RecordsPresenterViewInterface;
import com.example.threeinrow.util.RecordsAdapter;
import com.example.threeinrow.util.Util;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.util.List;

import moxy.MvpAppCompatFragment;
import moxy.presenter.InjectPresenter;

public class RecordsFragment extends MvpAppCompatFragment implements RecordsPresenterViewInterface {
    private View mView;
    private Target target;

    @InjectPresenter
    RecordsPresenter mPresenter;

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        OnBackPressedCallback callback = new OnBackPressedCallback(true) {
            @Override
            public void handleOnBackPressed() {
                getParentFragmentManager().popBackStack();
            }
        };
        requireActivity().getOnBackPressedDispatcher().addCallback(this, callback);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_records, container, false);
        setBackground();

        ImageButton backButton = mView.findViewById(R.id.btn_records_back);
        backButton.setOnClickListener(v -> getParentFragmentManager().popBackStack());
        return mView;
    }

    @Override
    public void fillRecords(List<Record> records) {
        RecyclerView recyclerView = mView.findViewById(R.id.recycler_records);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(new RecordsAdapter(records));
    }

    private void setBackground() {
        Bitmap background = Util.getBitmap(getContext(), "background.png");
        ConstraintLayout back = mView.findViewById(R.id.layout_records);
        if(background != null) back.setBackground(new BitmapDrawable(getResources(), background));
        else {
            target = new Target() {
                @Override
                public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                    back.setBackground(new BitmapDrawable(getResources(), bitmap));
                    Util.saveBitmap(getContext(), bitmap, "background.png", success -> {
                    });
                }

                @Override
                public void onBitmapFailed(Exception e, Drawable errorDrawable) {

                }

                @Override
                public void onPrepareLoad(Drawable placeHolderDrawable) {

                }
            };
            Picasso.get().load("http://159.69.89.54/ThreeInRow/backgrounds/default.png").into(target);
        }
    }
}