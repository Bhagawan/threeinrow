package com.example.threeinrow.fragment;

import androidx.appcompat.app.AlertDialog;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Rect;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.threeinrow.R;
import com.example.threeinrow.mvp.SavePresenter;
import com.example.threeinrow.mvp.SavePresenterViewInterface;


import java.util.Timer;
import java.util.TimerTask;

import moxy.MvpAppCompatActivity;
import moxy.presenter.InjectPresenter;

public class SaveActivity extends MvpAppCompatActivity implements SavePresenterViewInterface {

    @InjectPresenter
    SavePresenter mPresenter;

    @Override
    public void onBackPressed() {
        finish();
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_save);
        getWindow().getDecorView().setOnSystemUiVisibilityChangeListener(visibility -> fullscreen());
        fullscreen();

        int score = getIntent().getIntExtra("score", 0);

        EditText editText = findViewById(R.id.editText_save_input);
        Button negative = findViewById(R.id.btn_save_negative);
        negative.setOnClickListener(v -> mPresenter.negativeAction());
        Button positive = findViewById(R.id.btn_save_positive);
        positive.setOnClickListener(v -> mPresenter.positiveAction(editText.getText().toString(), score));

    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            View v = getCurrentFocus();
            if ( v instanceof EditText) {
                Rect outRect = new Rect();
                v.getGlobalVisibleRect(outRect);
                if (!outRect.contains((int)event.getRawX(), (int)event.getRawY())) {
                    v.clearFocus();
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                }
            }
        }
        fullscreen();
        return super.dispatchTouchEvent( event );
    }

    private void fullscreen() {
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LOW_PROFILE
                | View.SYSTEM_UI_FLAG_FULLSCREEN
                | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);

    }

    @Override
    public void showInput() {
        EditText editText = findViewById(R.id.editText_save_input);
        editText.setVisibility(View.VISIBLE);

        TextView header = findViewById(R.id.text_save_header);
        header.setText(getResources().getString(R.string.header_save_input));

        Button negative = findViewById(R.id.btn_save_negative);
        negative.setText(getResources().getString(R.string.action_cancel));
        Button positive = findViewById(R.id.btn_save_positive);
        positive.setText(getResources().getString(R.string.action_confirm));
    }

    @Override
    public void exit(boolean isSaved) {
        Intent intent = new Intent();
        if(isSaved) {
            intent.putExtra("result", 1);
            setResult(Activity.RESULT_OK, intent);
        } else {
            intent.putExtra("result", 0);
            setResult(Activity.RESULT_CANCELED, intent);
        }
        finish();
    }

    @Override
    public void serverError() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        View currAlert = getLayoutInflater().inflate(R.layout.alert_server_error, null);

        builder.setView(currAlert);

        AlertDialog alertDialog = builder.create();
        alertDialog.setOnShowListener(dialog -> fullscreen());
        alertDialog.show();

        Timer time  = new Timer();
        time.schedule(new TimerTask() {
            @Override
            public void run() {
                alertDialog.dismiss();
            }
        }, 2000);
    }

    @Override
    public void emptyName() {
        Toast.makeText(getApplicationContext(),
                getResources().getString(R.string.msg_empty_name_error),Toast.LENGTH_SHORT).show();
    }

}