package com.example.threeinrow.fragment;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;

import androidx.activity.OnBackPressedCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContract;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.example.threeinrow.R;
import com.example.threeinrow.util.Game;
import com.example.threeinrow.util.Util;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

public class GameFragment extends Fragment {
    private View mView;
    private int score = 0;
    Target target;

    ActivityResultContract<Integer, Integer> myContract = new ActivityResultContract<Integer, Integer>() {
        @NonNull
        @Override
        public Intent createIntent(@NonNull Context context, Integer input) {
            return new Intent(getContext(),SaveActivity.class).putExtra("score", input);
        }

        @Override
        public Integer parseResult(int resultCode, @Nullable Intent intent) {
            return intent.getIntExtra("result", 0);
        }
    };

    ActivityResultLauncher<Integer> launcher = registerForActivityResult(myContract, result -> {
        if(result == 1) getParentFragmentManager().popBackStack();
    });

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        OnBackPressedCallback callback = new OnBackPressedCallback(true) {
            @Override
            public void handleOnBackPressed() {
                getParentFragmentManager().popBackStack();
            }
        };
        requireActivity().getOnBackPressedDispatcher().addCallback(this, callback);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_game, container, false);
        setBackground();

        TextView scoreView = mView.findViewById(R.id.text_game_points);
        scoreView.setText(String.valueOf(score));
        Game game = mView.findViewById(R.id.game);
        game.addInterface(append -> {
            score += append;
            scoreView.setText(String.valueOf(score));
        });

        ImageButton backButton = mView.findViewById(R.id.btn_game_back);
        backButton.setOnClickListener(v -> launcher.launch(score));
        return mView;
    }

    private void setBackground() {
        Bitmap background = Util.getBitmap(getContext(), "background.png");
        ConstraintLayout back = mView.findViewById(R.id.layout_game);
        if(background != null) back.setBackground(new BitmapDrawable(getResources(), background));
        else {
            target = new Target() {
                @Override
                public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                    back.setBackground(new BitmapDrawable(getResources(), bitmap));
                    Util.saveBitmap(getContext(), bitmap, "background.png", success -> {
                    });
                }

                @Override
                public void onBitmapFailed(Exception e, Drawable errorDrawable) {

                }

                @Override
                public void onPrepareLoad(Drawable placeHolderDrawable) {

                }
            };
            Picasso.get().load("http://159.69.89.54/ThreeInRow/backgrounds/default.png").into(target);
        }
    }
}