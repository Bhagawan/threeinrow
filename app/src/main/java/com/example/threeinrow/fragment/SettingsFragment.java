package com.example.threeinrow.fragment;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;

import androidx.activity.OnBackPressedCallback;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.example.threeinrow.R;
import com.example.threeinrow.mvp.SettingsPresenter;
import com.example.threeinrow.mvp.SettingsPresenterViewInterface;
import com.example.threeinrow.util.SettingsAdapter;
import com.example.threeinrow.util.Util;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import moxy.MvpAppCompatFragment;
import moxy.presenter.InjectPresenter;

public class SettingsFragment extends MvpAppCompatFragment implements SettingsPresenterViewInterface {
    private View mView;
    private AlertDialog waitNotification;
    private Target target;

    @InjectPresenter
    SettingsPresenter mPresenter;

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        OnBackPressedCallback callback = new OnBackPressedCallback(true) {
            @Override
            public void handleOnBackPressed() {
                getParentFragmentManager().popBackStack();
            }
        };
        requireActivity().getOnBackPressedDispatcher().addCallback(this, callback);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_settings, container, false);
        setBackground();

        mPresenter.initialize(getContext());

        ImageButton backButton = mView.findViewById(R.id.btn_settings_back);
        backButton.setOnClickListener(v -> getParentFragmentManager().popBackStack());
        return mView;
    }

    @Override
    public void loadBackgrounds(List<Bitmap> previews) {
        RecyclerView recyclerView = mView.findViewById(R.id.recycler_settings_back);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
        SettingsAdapter adapter = new SettingsAdapter(previews);
        adapter.setListener(pos -> {
            setBackground();
            mPresenter.saveBackground(getContext(), previews.get(pos));
        });
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void loadForms(List<Bitmap> forms) {
        RecyclerView recyclerView = mView.findViewById(R.id.recycler_settings_forms);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
        SettingsAdapter adapter = new SettingsAdapter(forms);
        adapter.setListener(pos -> mPresenter.saveForm(getContext(), forms.get(pos)));
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void showWaitNotification() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());

        View currAlert = getLayoutInflater().inflate(R.layout.alert_server_error, null);
        TextView textView = currAlert.findViewById(R.id.text_alert);
        textView.setText(getResources().getString(R.string.msg_wait_notification));
        textView.setTextColor(getResources().getColor(R.color.white));
        builder.setView(currAlert);

        waitNotification = builder.create();
        waitNotification.setOnShowListener(dialog -> fullscreen());
        waitNotification.show();
    }

    @Override
    public void hideWaitNotification() {
        if(waitNotification.isShowing()) waitNotification.dismiss();
    }

    @Override
    public void showServerError() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());

        View currAlert = getLayoutInflater().inflate(R.layout.alert_server_error, null);

        builder.setView(currAlert);

        AlertDialog alertDialog = builder.create();
        alertDialog.setOnShowListener(dialog -> fullscreen());
        alertDialog.show();

        Timer time  = new Timer();
        time.schedule(new TimerTask() {
            @Override
            public void run() {
                alertDialog.dismiss();
            }
        }, 2000);
    }

    private void fullscreen() {
        getActivity().getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LOW_PROFILE
                | View.SYSTEM_UI_FLAG_FULLSCREEN
                | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);

    }

    private void setBackground() {
        Bitmap background = Util.getBitmap(getContext(), "background.png");
        ConstraintLayout back = mView.findViewById(R.id.layout_settings);
        if(background != null) back.setBackground(new BitmapDrawable(getResources(), background));
        else {
            target = new Target() {
                @Override
                public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                    back.setBackground(new BitmapDrawable(getResources(), bitmap));
                    Util.saveBitmap(getContext(), bitmap, "background.png", success -> {
                    });
                }

                @Override
                public void onBitmapFailed(Exception e, Drawable errorDrawable) {

                }

                @Override
                public void onPrepareLoad(Drawable placeHolderDrawable) {

                }
            };
            Picasso.get().load("http://159.69.89.54/ThreeInRow/backgrounds/default.png").into(target);
        }
    }
}