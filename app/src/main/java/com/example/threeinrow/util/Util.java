package com.example.threeinrow.util;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Handler;
import android.os.Looper;

import com.example.threeinrow.data.Name;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Util {

    public static Bitmap paintBitmap(Bitmap bitmap, int color) {
        Bitmap temp = Bitmap.createBitmap(bitmap.getWidth(),bitmap.getHeight(), Bitmap.Config.ARGB_8888);
        float[] pHSV = new float[3];
        float[] dstHSV = new float[3];
        Color.colorToHSV(color, dstHSV);
        for(int i = 0; i < bitmap.getWidth(); i++) {
            for (int j = 0; j < bitmap.getHeight(); j++) {
                int pixelColor = bitmap.getPixel(i, j);
                int pixelAlpha = Color.alpha(pixelColor);
                Color.colorToHSV(pixelColor, pHSV);
                dstHSV[1] = pHSV[1];
                dstHSV[2] = pHSV[2];
                if (pixelColor != Color.WHITE)
                    temp.setPixel(i, j, Color.HSVToColor(pixelAlpha, dstHSV));
                else temp.setPixel(i, j, pixelColor);
            }
        }
        return temp;
    }

    public static void downloadBitmaps(String path, List<Name> names, int x, int y, Util.DownloadStatus callback) {
        ExecutorService executor = Executors.newSingleThreadExecutor();
        Handler handler = new Handler(Looper.getMainLooper());
        List<Bitmap> bitmaps = new ArrayList<>();

        executor.execute(() -> {
            try {
                for(Name name: names) {
                    String file = path + name.name;
                    bitmaps.add(Picasso.get().load(file).resize(x,y).get());
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            handler.post(() -> callback.onComplete(bitmaps));
        });
    }

    public static void saveBitmap(Context context, Bitmap bitmap, String name, Util.Result callback) {
        ExecutorService executor = Executors.newSingleThreadExecutor();
        Handler handler = new Handler(Looper.getMainLooper());

        executor.execute(() -> {
            File dir = new File(context.getFilesDir() + "/style");
            if (!dir.exists())
                dir.mkdir();
            File file = new File(dir, name);
            FileOutputStream outputStream = null;
            try {
                file.createNewFile();
                outputStream = new FileOutputStream(file, false);
                bitmap.compress(Bitmap.CompressFormat.PNG, 100, outputStream);
                outputStream.close();

            } catch (IOException e) {
                callback.sendResult(false);
                e.getStackTrace();
            }
            handler.post(() -> callback.sendResult(true));
        });
    }

    public static Bitmap getBitmap(Context context, String name) {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inPreferredConfig = Bitmap.Config.ARGB_8888;
        String path = context.getFilesDir() + "/style/" + name;

        return BitmapFactory.decodeFile(path, options);
    }

    public interface Result {
        void sendResult(boolean success);
    }

    public interface DownloadStatus {
        void onComplete(List<Bitmap> bitmaps);
    }
}


