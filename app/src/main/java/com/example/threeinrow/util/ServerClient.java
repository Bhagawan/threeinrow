package com.example.threeinrow.util;

import com.example.threeinrow.data.Name;
import com.example.threeinrow.data.Record;
import com.example.threeinrow.data.SplashResponse;

import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface ServerClient {

    @FormUrlEncoded
    @POST("ThreeInRow/addScore.php")
    Call<ResponseBody> sendRecord( @Field("score") int score, @Field("name") String name);

    @GET("ThreeInRow/score.json")
    Call<List<Record>> getRecords();

    @GET("ThreeInRow/{file}")
    Call<List<Name>> getNames(@Path("file") String file);

    @FormUrlEncoded
    @POST("ThreeInRow/splash.php")
    Call<SplashResponse> getSplash(@Field("locale") String locale);

}
