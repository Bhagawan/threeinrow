package com.example.threeinrow.util;

import android.graphics.Point;

public class MyAnimation {
    private float startX, startY, endX, endY;
    private int type;
    private double speedX, speedY;
    private boolean active = true;

    public MyAnimation(int startX, int startY, int endX, int endY, int type, int speed) {
        this.startX = startX;
        this.startY = startY;
        this.endX = endX;
        this.endY = endY;
        this.type = type;

        float tempX = endX - startX;
        float tempY = endY - startY;
        double l = Math.sqrt(Math.pow(tempX, 2) + Math.pow(tempY, 2));

        this.speedX = (tempX / l * speed);
        this.speedY = (tempY / l * speed);
    }

    public Point getNextPoint() {
        Point p = new Point((int)startX, (int)startY);

        float tempX = endX - startX;
        float tempY = endY - startY;

        if(Math.abs(tempX) <= speedX && Math.abs(tempY) <= speedY) {
            startX = endX;
            startY = endY;
            active = false;
        } else {
            startX = (float) (startX + (speedX * Math.signum(tempX)));
            startY = (float) (startY + (speedY * Math.signum(tempY)));
        }
        return p;
    }

    public int getType() {
        return type;
    }

    public boolean isActive() {
        return active;
    }
}
