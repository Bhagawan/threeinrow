package com.example.threeinrow.util;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.threeinrow.R;
import com.example.threeinrow.data.Record;

import java.util.List;

public class RecordsAdapter extends RecyclerView.Adapter<RecordsAdapter.ViewHolder> {
    private List<Record> records;

    public RecordsAdapter(List<Record> data) {
        this.records = data;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView name;
        private TextView score;
        private TextView place;


        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            place = itemView.findViewById(R.id.item_record_place);
            name = itemView.findViewById(R.id.item_record_name);
            score = itemView.findViewById(R.id.item_record_score);
        }
    }

    @NonNull
    @Override
    public RecordsAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_records, parent, false);
        return new RecordsAdapter.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull RecordsAdapter.ViewHolder holder, int position) {
        Record record = records.get(position);
        holder.place.setText(String.valueOf(position + 1));
        holder.name.setText(record.getName());
        holder.score.setText(String.valueOf(record.getScore()));
    }

    @Override
    public int getItemCount() {
        if(records == null) return 0;
        else return records.size();
    }
}