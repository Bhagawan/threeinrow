package com.example.threeinrow.util;

import android.graphics.Point;

import java.util.ArrayList;

public class Figure {
    private int type;
    private ArrayList<Point> elements = new ArrayList<>();

    public Figure(int type) {
        this.type = type;
    }

    public void addPoint(int x, int y) { elements.add(new Point(x, y)); }


    public int getType() {
        return type;
    }

    public Point getElement(int n) { return elements.get(n); }

    public int getSize() { return elements.size(); }


}
