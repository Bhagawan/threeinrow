package com.example.threeinrow.util;

import android.graphics.Point;

public class ElementAnimation {
    private int x, y, speed;
    private float currX, currY, startX, startY, endX, endY;
    private double speedX, speedY;
    private boolean active = true;

    public ElementAnimation(int x, int y, int startX, int startY, int endX, int endY, int speed) {
        this.x = x;
        this.y = y;
        this.speed = speed;
        this.startX = startX;
        this.startY = startY;
        this.endX = endX;
        this.endY = endY;

        float tempX = endX - startX;
        float tempY = endY - startY;

        currX = startX;
        currY = startY;
        double l = Math.sqrt(Math.pow(tempX, 2) + Math.pow(tempY, 2));

        this.speedX = ((tempX / l) * speed);
        this.speedY = ((tempY / l) * speed);
    }

    public Point getNextPoint() {
        Point p = new Point((int)currX, (int)currY);
        //Point p = new Point((int)(currX - startX), (int)(currY - startY));

        if(Math.abs(currX - endX) <= Math.abs(speedX) && Math.abs(currY - endY) <= Math.abs(speedY)) {
            active = false;
            return new Point((int)endX, (int)endY);
        } else {
            currX += speedX;
            currY += speedY;
        }

        return p;
    }
    public boolean isActive() {
        return active;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public Point getCurrent() {
        return  new Point((int)currX, (int)currY);
    }

    public void setEndPoint(float x, float y) {
        this.endX = x;
        this.endY = y;
        float tempX = endX - currX;
        float tempY = endY - currY;
        double l = Math.sqrt(Math.pow(tempX, 2) + Math.pow(tempY, 2));

        this.speedX = ((tempX / l) * speed);
        this.speedY = ((tempY / l) * speed);
    }

    public float getEndX() {
        return endX;
    }

    public float getEndY() {
        return endY;
    }
}
