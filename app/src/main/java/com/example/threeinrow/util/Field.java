package com.example.threeinrow.util;

import java.util.ArrayList;
import java.util.Random;

public class Field {
    private int[][] field;
    private int width, height;
    private boolean[][] checked;
    private int typeAmount;

    public Field(int width, int height, int typeAmount) {
        this.width = width;
        this.height = height;
        this.typeAmount = typeAmount;
        checked = new boolean[width][height];
        Random random = new Random();
        field = new int[width][height];
        for(int y = 0; y < height; y++)
            for(int x = 0; x < width; x++) {
                field[x][y] = random.nextInt(typeAmount);
            }

        ArrayList<Figure> figures = getFigures();
        while(figures.size() > 0) {
            for(Figure figure: figures){
                for(int i = 0; i < figure.getSize(); i++){
                    field[figure.getElement(i).x][figure.getElement(i).y] = random.nextInt(typeAmount);
                }
            }
            figures = getFigures();
        }
    }

    public Field(Field field) {
        this.width = field.getWidth();
        this.height = field.getHeight();
        this.field = field.getField();
        this.checked = field.getChecked();
        this.typeAmount = field.getTypeAmount();
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public int getType(int x, int y) {
        return field[x][y];
    }

    public void moveElementTo(int firstX, int firstY, int secondX, int secondY) {
        if(onField(firstX, firstY) && onField(secondX, secondY))
        if(firstX == secondX) {
            if(firstY > secondY) {
                int temp = field[firstX][firstY];
                for(int i = firstY; i > secondY ; i--) {
                    field[firstX][i] = field[secondX][i - 1];
                }
                field[secondX][secondY] = temp;
            } else if (firstY < secondY) {
                int temp = field[firstX][firstY];
                for(int i = firstY; i < secondY ; i++) {
                    field[secondX][i] = field[secondX][i + 1];
                }
                field[secondX][secondY] = temp;
            }
        } else if(firstY == secondY) {
            if(firstX > secondX) {
                int temp = field[firstX][firstY];
                for(int i = firstX; i > secondX; i--) {
                    field[i][firstY] = field[i - 1][firstY];
                }
                field[secondX][secondY] = temp;
            } else {
                int temp = field[firstX][firstY];
                for(int i = firstX; i < secondX ; i++) {
                    field[i][firstY] = field[i + 1][firstY];
                }
                field[secondX][secondY] = temp;
            }
        }
    }

    public void delete(int x, int y) {
        if(onField(x, y)) {
            if(y >= 1) {
                for(int i = y - 1; i >= 0; i--) {
                    field[x][i + 1] = field[x][i];
                }
            }
            Random random = new Random();
            field[x][0] = random.nextInt(typeAmount);
        }
    }

    public void set(int x, int y, int type) {
        field[x][y] = type;
    }

    public boolean onField(int x, int y) {
        return x >= 0 && x < width && y >= 0 && y < height;
    }

    public ArrayList<Figure> getFigures() {
        ArrayList<Figure> figures = new ArrayList<>();
        clearChecked();

        for(int i = 0; i < width; i++)
            for(int j = 0; j < height; j++)
                check(i, j);

        for(int i = 0; i < width; i++) {
            for(int j = 0; j < height; j++) {
                if(checked[i][j]) {
                    Figure figure = new Figure(field[i][j]);
                    fillFigure(i, j, figure);
                    figures.add(figure);
                }
            }
        }
        return figures;
    }

    public int[][] getField() {
        int[][] f = new int[width][height];
        for(int y = 0; y < height; y++)
            for(int x = 0; x < width; x++) {
                f[x][y] = this.field[x][y];
            }
        return f;
    }

    public boolean[][] getChecked() {
        return checked;
    }

    public int getTypeAmount() {
        return typeAmount;
    }

    private void fillFigure(int x, int y, Figure figure) {
        figure.addPoint(x, y);
        checked[x][y] = false;
        if(onField(x - 1, y))
            if(checked[x - 1][y] && field[x - 1][y] == field[x][y])
                fillFigure(x - 1, y, figure);
        if(onField(x + 1, y))
            if(checked[x + 1][y] && field[x + 1][y] == field[x][y])
                fillFigure(x + 1, y, figure);
        if(onField(x, y - 1))
            if(checked[x][y - 1] && field[x][y - 1] == field[x][y])
                fillFigure(x, y - 1, figure);
        if(onField(x, y + 1))
            if(checked[x][y + 1] && field[x][y + 1] == field[x][y])
                fillFigure(x, y + 1, figure);
    }

    private void check(int x, int y) {
        if(x > 0 && x <= width - 2)
            if(field[x - 1][y] == field[x][y] && field[x + 1][y] == field[x][y]) {
                checked[x - 1][y] = true;
                checked[x + 1][y] = true;
                checked[x][y] = true;
            }
        if(y > 0 && y <= height - 2)
            if(field[x][y - 1] == field[x][y] && field[x][y + 1] == field[x][y]) {
                checked[x][y - 1] = true;
                checked[x][y + 1] = true;
                checked[x][y] = true;
            }
    }

    private void clearChecked() {
        for(int i = 0; i < width; i++)
            for(int j = 0; j < height; j++)
                checked[i][j] = false;
    }

}
