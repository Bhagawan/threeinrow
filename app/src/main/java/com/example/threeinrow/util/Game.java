package com.example.threeinrow.util;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Point;
import android.os.Handler;
import android.os.Looper;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

import androidx.annotation.Nullable;

import com.example.threeinrow.R;

import java.util.ArrayList;
import java.util.Vector;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Game extends View {
    private final int imgSize = 50;
    private final int typeAmount = 5;
    private final int moveSpeed = 20;
    private final int fallSpeed = 20;

    private float mHeight;
    private float mWidth;
    private Bitmap[] balls = new Bitmap[typeAmount];
    private Field field;
    private int paddingLeft, paddingTop;
    private int mColumn = -1, mRow = -1;
    private float lastX, lastY, firstX, firstY;
    private float movePaddTop, movePaddLeft;
    private Point[][] eShift;
    private Point shift = new Point(0,0);
    private ElementAnimation[][] animations;
    private ElementAnimation[][] fallAnimations;
    private GameInterface gameInterface;



    public Game(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);

        ExecutorService executor = Executors.newSingleThreadExecutor();
        Handler handler = new Handler(Looper.getMainLooper());


        executor.execute(() -> {
            while(true) {
                try {
                    handler.post(this::invalidate);
                    Thread.sleep(1000 / 60);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
    }


    @Override
    protected void onDraw(Canvas c) {
        if(field != null) {
            drawField(c);
            drawMovement(c);
        }
    }

    @Override
    protected void onSizeChanged(int xNew, int yNew, int xOld, int yOld) {
        super.onSizeChanged(xNew, yNew, xOld, yOld);
        if(getWidth() > 0) initialise();
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if(event.getActionMasked() == MotionEvent.ACTION_DOWN) {
            mColumn = getColumn(event.getX());
            mRow = getRow(event.getY());
            lastX = event.getX();
            lastY = event.getY();
            firstX = event.getX();
            firstY = event.getY();
            movePaddLeft = firstX - paddingLeft - (mColumn * imgSize);
            movePaddTop = firstY - paddingTop - (mRow * imgSize);
            return true;
        } else if(event.getActionMasked() == MotionEvent.ACTION_MOVE) {
            lastX = event.getX();
            lastY = event.getY();
            return true;
        } else if(event.getActionMasked() == MotionEvent.ACTION_UP) {
            checkElements();
            mColumn = -1;
            mRow = -1;
            shift.x = 0;
            return true;
        }
        return super.onTouchEvent(event);
    }

    public void addInterface( GameInterface gameInterface) {
        this.gameInterface = gameInterface;
    }

    private void initialise() {
        mWidth = getWidth();
        mHeight = getHeight();
        int sizeX = (int)(mWidth / imgSize);
        int sizeY = (int)(mHeight / imgSize);
        paddingLeft = (int) ((mWidth % imgSize) / 2);
        paddingTop = (int) ((mHeight % imgSize) / 2);
        field = new Field(sizeX, sizeY, typeAmount);
        animations = new ElementAnimation[sizeX][sizeY];
        fallAnimations = new ElementAnimation[sizeX][sizeY];
        eShift = new Point[sizeX][sizeY];
        for(int i = 0; i < sizeX; i++) {
            for (int j = 0; j < sizeY; j++) {
                eShift[i][j] = new Point(0, 0);
            }
        }
        Bitmap ball = Util.getBitmap(getContext(), "form.png");
        if(ball == null) ball = Bitmap.createScaledBitmap(BitmapFactory.decodeResource(getResources(),
                R.drawable.ball), imgSize, imgSize,false);
        else ball = Bitmap.createScaledBitmap(ball,imgSize, imgSize,false);

//        Random rand = new Random();
//        for(int i = 0; i < balls.length; i++) {
//            int r = rand.nextInt(255);
//            int g = rand.nextInt(255);
//            int b = rand.nextInt(255);
//            balls[i] = Util.paintBitmap(ball, Color.rgb(r, g, b));
//        }

        balls[0] = Util.paintBitmap(ball, Color.RED);
        balls[1] = Util.paintBitmap(ball, Color.YELLOW);
        balls[2] = Util.paintBitmap(ball, Color.GREEN);
        balls[3] = Util.paintBitmap(ball, Color.BLUE);
        balls[4] = Util.paintBitmap(ball, Color.parseColor("#F33A00"));
    }

    private void drawField(Canvas c) {
        Point offset = new Point(0, 0);
        for(int y = 0; y < field.getHeight(); y++)
            for (int x = 0; x < field.getWidth(); x++) {
                if(!(mColumn == x && mRow == y) && balls[field.getType(x,y)] != null && field.getType(x, y) != -1)
                    if(fallAnimations[x][y] != null) {
                        offset = fallAnimations[x][y].getNextPoint();
                        c.drawBitmap(balls[field.getType(x,y)], offset.x, offset.y, null);
                        if(!fallAnimations[x][y].isActive()) fallAnimations[x][y] = null;
                    }
                    else if(animations[x][y] != null) {
                        if(!isShifted(x, y)){
                            animations[x][y] = null;
                            c.drawBitmap(balls[field.getType(x,y)],paddingLeft + ( x * imgSize),paddingTop + (y * imgSize), null);
                        } else {
                            offset = animations[x][y].getNextPoint();
                            c.drawBitmap(balls[field.getType(x,y)], offset.x, offset.y, null);
                        }
                    }
                    else {
                        c.drawBitmap(balls[field.getType(x,y)],paddingLeft + ( x * imgSize),paddingTop + (y * imgSize), null);
                    }
                    //c.drawBitmap(balls[field.getType(x,y)],paddingLeft + ( x * imgSize) + offset.x,paddingTop + (y * imgSize) + offset.y, null);
            }
    }

    private int getRow(float y) {
        return (int) (y - paddingTop) / imgSize;
    }

    private int getColumn(float x) {
        return (int) (x - paddingLeft) / imgSize;
    }

    private void drawMovement(Canvas c) {
        if (field.onField(mColumn, mRow)) {
            float dX = lastX - firstX;
            float dY = lastY - firstY;
            boolean b = Math.abs(dX) < Math.abs(dY);
            if (shift.x == 0) {
                if (b) {
                    shift.x = 1; //vertical
                    int a = getRow(lastY) - mRow;
                    if((mRow + a) > (field.getHeight() -1)) a = field.getHeight() - mRow - 1;
                    if((mRow + a) < 0) a = -mRow;
                    shift.y = a;
                    if (shift.y > 0)
                        for (int i = 1; i <= shift.y; i++)
                            moveUp(mColumn, mRow + i, 1, moveSpeed);
                    else
                        for (int i = -1; i >= shift.y; i--)
                            moveDown(mColumn, mRow + i, 1, moveSpeed);
                } else {
                    shift.x = 2;
                    int a = getColumn(lastX) - mColumn;
                    if((mColumn + a) > (field.getWidth() -1)) a = field.getWidth() - mColumn - 1;
                    if((mColumn + a) < 0) a = -mColumn;
                    shift.y = a;
                    if (shift.y > 0) {
                        for (int i = 1; i <= shift.y; i++) {
                            moveLeft(mColumn + i, mRow, 1, moveSpeed);
                        }
                    }
                    else {
                        for (int i = -1; i >= shift.y; i--)
                            moveRight(mColumn + i, mRow, 1, moveSpeed);
                    }
                }
            } else {
                if (b) {    //vertical
                    if (shift.x == 1) {
                        int a = getRow(lastY) - mRow;
                        if((mRow + a) > (field.getHeight() -1)) a = field.getHeight() - mRow - 1;
                        if((mRow + a) < 0) a = -mRow;
                        if(Math.abs(a) < Math.abs(shift.y)) {
                            if (a > shift.y)
                                for (int i = shift.y; i < a; i++)
                                    moveHome(mColumn, mRow + i, moveSpeed);
                            else
                                for (int i = shift.y; i > a; i--)
                                    moveHome(mColumn, mRow + i, moveSpeed);
                        } else {
                            if (a > shift.y)
                                for (int i = shift.y + 1; i <= a; i++)
                                    moveUp(mColumn, mRow + i, 1, moveSpeed);
                            else
                                for (int i = shift.y - 1; i >= a; i--)
                                    moveDown(mColumn, mRow + i, 1, moveSpeed);
                        }
                        shift.y = a;
                    } else if (shift.x == 2) {
                        shift.x = 1;
                        int a = getRow(lastY) - mRow;
                        if((mRow + a) > (field.getHeight() -1)) a = field.getHeight() - mRow - 1;
                        if((mRow + a) < 0) a = -mRow;
                        shift.y = a;
                        if (shift.y > 0)
                            for (int i = 1; i <= shift.y; i++)
                                moveUp(mColumn, mRow + i, 1, moveSpeed);
                        else
                            for (int i = -1; i >= shift.y; i--)
                                moveDown(mColumn, mRow + i, 1, moveSpeed);
                    }
                } else {    //horizontal
                    if (shift.x == 1) {
                        shift.x = 2;
                        int a = getColumn(lastX) - mColumn;
                        if((mColumn + a) > (field.getWidth() -1)) a = field.getWidth() - mColumn - 1;
                        if((mColumn + a) < 0) a = - mColumn;
                        shift.y = a;
                        if (shift.y > 0)
                            for (int i = 1; i <= shift.y; i++)
                                moveLeft(mColumn + i, mRow, 1, moveSpeed);
                        else
                            for (int i = -1; i >= shift.y; i--)
                                moveRight(mColumn + i, mRow, 1, moveSpeed);
                    } else if (shift.x == 2) {
                        int a = getColumn(lastX) - mColumn;
                        if((mColumn + a) > (field.getWidth() -1)) a = field.getWidth() - mColumn - 1;
                        if((mColumn + a) < 0) a = - mColumn;
                        if(Math.abs(a) < Math.abs(shift.y)) {
                            if (a > shift.y)
                                for (int i = shift.y; i < a; i++)
                                    moveHome(mColumn + i, mRow, moveSpeed);
                            else
                                for (int i = shift.y ; i > a; i--)
                                    moveHome(mColumn + i, mRow, moveSpeed);
                        } else {
                            if (a > shift.y) {
                                for (int i = shift.y + 1; i <= a; i++)
                                    moveLeft(mColumn + i, mRow, 1, moveSpeed);
                            } else
                                for (int i = shift.y - 1; i >= a; i--)
                                    moveRight(mColumn + i, mRow, 1, moveSpeed);
                        }
                        shift.y = a;
                    }
                }
            }


            if (b) {
                float moveDX = firstX - movePaddLeft;
                float moveDY = lastY - movePaddTop;
                if(moveDY > mHeight - imgSize - paddingTop) moveDY = mHeight - imgSize - paddingTop;
                if(moveDY < paddingTop) moveDY = paddingTop;
                c.drawBitmap(balls[field.getType(mColumn, mRow)], moveDX, moveDY, null);

            } else {
                float moveDX = lastX - movePaddLeft;
                float moveDY = firstY - movePaddTop;
                if(moveDX > mWidth - imgSize - paddingLeft) moveDX =  mWidth - imgSize - paddingLeft;
                if(moveDX < paddingLeft) moveDX = paddingLeft;
                c.drawBitmap(balls[field.getType(mColumn, mRow)], moveDX, moveDY , null);
            }
        }
    }

    private void switchElements(Field f) {
            float dX = lastX - firstX;
            float dY = lastY - firstY;
            if(Math.abs(dX) < Math.abs(dY)) {
                int row = getRow(lastY);
                if(row >= f.getWidth()) row = f.getWidth() - 1;
                if(row < 0) row = 0;
                f.moveElementTo(mColumn, mRow, mColumn, row);
            } else {
                int column = getColumn(lastX);
                if(column >= f.getHeight()) column = f.getHeight() - 1;
                if(column < 0) column = 0;
                f.moveElementTo(mColumn, mRow, column, mRow);
            }
    }

    private void moveUp(int x, int y, int n, int speed) {
        if(animations[x][y] != null){
            animations[x][y].setEndPoint(imgSize * x + paddingLeft
                    ,imgSize * (y - n) + paddingTop);
        }
        else animations[x][y] = new ElementAnimation(x, y, imgSize * x + paddingLeft
                ,imgSize * y + paddingTop,  imgSize * x + paddingLeft
                ,imgSize * (y - n) + paddingTop, speed);
    }

    private void moveDown(int x, int y, int n, int speed) {
        try{


        if(animations[x][y] != null) animations[x][y].setEndPoint(imgSize * x + paddingLeft
                ,imgSize * (y + n) + paddingTop);
        else animations[x][y] = new ElementAnimation(x, y, imgSize * x + paddingLeft
                ,imgSize * y + paddingTop,  imgSize * x + paddingLeft
                ,imgSize * (y + n) + paddingTop, speed);
        } catch (ArrayIndexOutOfBoundsException e){
            e.printStackTrace();
        }

    }

    private void moveLeft(int x, int y, int n, int speed) {
        if(animations[x][y] != null) animations[x][y].setEndPoint(imgSize * (x - n) + paddingLeft
                ,imgSize * y  + paddingTop);
        else animations[x][y] = new ElementAnimation(x, y, imgSize * x + paddingLeft
                ,imgSize * y + paddingTop ,  imgSize * (x - n) + paddingLeft
                ,imgSize * y  + paddingTop, speed);
    }

    private void moveRight(int x, int y, int n, int speed) {
        if(animations[x][y] != null) animations[x][y].setEndPoint(imgSize * (x + n) + paddingLeft
                ,imgSize * y + paddingTop);
        else animations[x][y] = new ElementAnimation(x, y, imgSize * x + paddingLeft
                ,imgSize * y + paddingTop ,  imgSize * (x + n) + paddingLeft
                ,imgSize * y  + paddingTop, speed);
    }

    private void moveHome(int x, int y, int speed) {
        if(animations[x][y] != null) animations[x][y].setEndPoint(imgSize * x  + paddingLeft
                ,imgSize * y + paddingTop);
    }

    private void fall(int x, int y) {
        if(fallAnimations[x][y] != null) fallAnimations[x][y].setEndPoint(imgSize * x  + paddingLeft
                ,imgSize * y + paddingTop);
        else fallAnimations[x][y] = new ElementAnimation(x, y, imgSize * x + paddingLeft
                ,imgSize * (y - 1) + paddingTop ,  imgSize * x  + paddingLeft
                ,imgSize * y  + paddingTop, fallSpeed);
    }

    private boolean isShifted(int x, int y) {
        if(shift.x == 1 && x == mColumn) {
            if (shift.y > 0) {
                if (y > mRow && y <= mRow + shift.y) return true;
            } else if (y >= mRow + shift.y && y < mRow) return true;
        }
        else if(shift.x == 2 && y == mRow) {
            if (shift.y > 0) {
                if (x > mColumn && x <= mColumn + shift.y) return true;
            } else if (x >= mColumn + shift.y && x < mColumn) return true;
        }
        return false;
    }

    private void checkElements() {
        Field test = new Field(field);
        switchElements(test);
        if(test.getFigures().size() > 0) {
            switchElements(field);
            ArrayList<Figure> match = field.getFigures();
            while(match.size() > 0) {
                for(Figure figure: match) {
                    if(gameInterface != null) gameInterface.scoreChange(calculateScore(figure.getSize()));
                    for(int i = 0; i < figure.getSize(); i++) {
                        deleteElement(figure.getElement(i).x, figure.getElement(i).y);
                    }
                }
                match = field.getFigures();
            }
        }
    }

    private void deleteElement(int x, int y) {
        field.delete(x, y);
        for(int i = y - 1; i >= 0; i--) {
            fall(x, i);
        }
    }

    private int calculateScore(int num) {
        if(num <= 3) return num;
        else {
            int s = 3;
            for(int i = num - 3; i <= num; i++) s += i;
            return s;
        }
    }

    public interface GameInterface {
        public void scoreChange(int append);
    }

}
