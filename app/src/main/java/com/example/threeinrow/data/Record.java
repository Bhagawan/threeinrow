package com.example.threeinrow.data;

import com.google.gson.annotations.SerializedName;

public class Record {

    @SerializedName("name")
    private String name;
    @SerializedName("score")
    private int score;

    public String getName() {
        return name;
    }

    public int getScore() {
        return score;
    }
}
