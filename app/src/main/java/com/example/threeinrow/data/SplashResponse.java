package com.example.threeinrow.data;

import com.google.gson.annotations.SerializedName;

public class SplashResponse {
    @SerializedName("url")
    public String url;
}
