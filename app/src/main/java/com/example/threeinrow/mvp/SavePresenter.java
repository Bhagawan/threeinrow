package com.example.threeinrow.mvp;

import androidx.annotation.NonNull;

import com.example.threeinrow.util.MyServerClient;
import com.example.threeinrow.util.ServerClient;


import moxy.InjectViewState;
import moxy.MvpPresenter;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;

@InjectViewState
public class SavePresenter extends MvpPresenter<SavePresenterViewInterface> {
    private boolean input = false;

    public void positiveAction(String name, int score) {
        if(input) {
            if(name.isEmpty()) getViewState().emptyName();
            else {
                ServerClient serverClient = MyServerClient.createService(ServerClient.class);
                Call<ResponseBody> call = serverClient.sendRecord(score, name);
                call.enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(@NonNull Call<ResponseBody> call, @NonNull  retrofit2.Response<ResponseBody> response) {
                        getViewState().exit(true);
                    }

                    @Override
                    public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {
                        getViewState().serverError();
                    }
                });
            }
        } else {
            input = true;
            getViewState().showInput();
        }
    }

    public void negativeAction() {
        if(input) {
            getViewState().exit(false);
        } else getViewState().exit(true);
    }
}
