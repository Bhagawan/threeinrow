package com.example.threeinrow.mvp;

import moxy.MvpView;
import moxy.viewstate.strategy.alias.SingleState;

public interface SavePresenterViewInterface extends MvpView {

    @SingleState
    void showInput();

    @SingleState
    void exit(boolean isSaved);

    @SingleState
    void serverError();

    @SingleState
    void emptyName();

}
