package com.example.threeinrow.mvp;

import com.example.threeinrow.data.Record;

import java.util.List;

import moxy.MvpView;
import moxy.viewstate.strategy.alias.SingleState;

public interface RecordsPresenterViewInterface extends MvpView {
    @SingleState
    void fillRecords(List<Record> forms);
}
