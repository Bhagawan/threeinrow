package com.example.threeinrow.mvp;

import android.graphics.Bitmap;

import java.util.List;

import moxy.MvpView;
import moxy.viewstate.strategy.alias.OneExecution;
import moxy.viewstate.strategy.alias.SingleState;

public interface SettingsPresenterViewInterface extends MvpView {
    @SingleState
    void loadBackgrounds(List<Bitmap> previews);

    @SingleState
    void loadForms(List<Bitmap> forms);

    @OneExecution
    void showWaitNotification();

    @OneExecution
    void hideWaitNotification();

    @OneExecution
    void showServerError();

}
