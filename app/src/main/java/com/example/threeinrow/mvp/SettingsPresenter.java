package com.example.threeinrow.mvp;

import android.content.Context;
import android.graphics.Bitmap;

import androidx.annotation.NonNull;

import com.example.threeinrow.data.Name;
import com.example.threeinrow.util.MyServerClient;
import com.example.threeinrow.util.ServerClient;
import com.example.threeinrow.util.Util;

import java.util.List;

import moxy.InjectViewState;
import moxy.MvpPresenter;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

@InjectViewState
public class SettingsPresenter extends MvpPresenter<SettingsPresenterViewInterface> {
    private static final String URL = "http://159.69.89.54/";

    public void initialize(Context context) {
        ServerClient serverClient = MyServerClient.createService(ServerClient.class);
        Call<List<Name>> backgrounds = serverClient.getNames("backgrounds.json");
        backgrounds.enqueue(new Callback<List<Name>>() {
            @Override
            public void onResponse(@NonNull Call<List<Name>> call, @NonNull Response<List<Name>> response) {
                if(response.body() != null) downloadBackgrounds(response.body());
            }

            @Override
            public void onFailure(@NonNull Call<List<Name>> call, @NonNull Throwable t) {

            }
        });

        Call<List<Name>> forms = serverClient.getNames("forms.json");
        forms.enqueue(new Callback<List<Name>>() {
            @Override
            public void onResponse(@NonNull Call<List<Name>> call, @NonNull Response<List<Name>> response) {
                if(response.body() != null) downloadForms(response.body());
            }

            @Override
            public void onFailure(@NonNull Call<List<Name>> call, @NonNull Throwable t) {

            }
        });
    }

    public void saveBackground(Context context, Bitmap bitmap) {
        getViewState().showWaitNotification();
        Util.saveBitmap(context, bitmap, "background.png", success -> {
            if(success) {
                getViewState().hideWaitNotification();
            }
            else getViewState().showServerError();
        });
    }

    public void saveForm(Context context, Bitmap bitmap) {
        getViewState().showWaitNotification();
        Util.saveBitmap(context, bitmap, "form.png", success -> {
            if(success) getViewState().hideWaitNotification();
            else getViewState().showServerError();
        });
    }

    private void downloadBackgrounds(List<Name> names){
        String url = URL + "ThreeInRow/backgrounds/";
        Util.downloadBitmaps(url, names, 150, 200, bitmaps -> getViewState().loadBackgrounds(bitmaps));
    }

    private void downloadForms(List<Name> names){
        String url = URL + "ThreeInRow/forms/";
        Util.downloadBitmaps(url, names, 100, 100, bitmaps -> getViewState().loadForms(bitmaps));
    }
}
